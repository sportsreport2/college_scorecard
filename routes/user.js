var passport = require('passport');
var settings = require('../config/settings');
require('../config/passport')(passport);
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var User = require("../models/User");

//This route gets all users
router.get('/', passport.authenticate('jwt', { session: false}), function(req, res) {
    var token = getToken(req.headers);
    if (token) {
        User.find().select({
            'firstname': 1,
            'lastname': 1,
            'username': 1
        }).exec(function (err, users) {
            if (err) return next(err);
            res.json({
                success: true,
                msg: 'Success',
                results: users
            });
        });
    } else {
        return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
});

//This route creates a user
router.post('/', function(req, res) {
    createUser(req, res);
});

//This route gets a user by ID
router.get('/:id', function(req, res) {
    var token = getToken(req.headers);
    if (token) {
        User.findOne({
            '_id': req.params.id
        }).select({
            'firstname': 1,
            'lastname': 1,
            'username': 1
        }).exec(function (err, user) {
            if (err) return next(err);
            res.json({
                success: true,
                msg: 'Success',
                results: user
            });
        });
    } else {
        return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
});

//This route updates a user by ID
router.put('/:id', function(req, res) {
    var token = getToken(req.headers);
    if (token) {
        //Don't allow password to be updated in this request
        if(req.body.hasOwnProperty("password")) {
            delete req.body.password
        }

        User.findByIdAndUpdate(
            req.params.id,
            req.body,
            {new: true},
            function (err, user) {
                if (err) return next(err);
                res.json({
                    success: true,
                    msg: 'User updated successfully.',
                    results: user
                });
            }
        )
    } else {
        return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
});

//This route deletes a user by ID
router.delete('/:id', function(req, res) {
    var token = getToken(req.headers);
    if (token) {
        User.findByIdAndRemove(
            req.params.id,
            function (err, user) {
                if (err) return next(err);
                res.json({
                    success: true,
                    msg: 'Successfuly deleted user',
                    results: user
                });
            }
        )
    } else {
        return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
});

//This function gets a jwt token from the request headers
getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

createUser = function (req, res, requireAuth) {
    requireAuth = requireAuth || false

    if(requireAuth) {
        var token = getToken(req.headers)
        if(!token) {
            return res.status(403).send({success: false, msg: 'Unauthorized.'});
        }
    }

    if (!req.body.username || !req.body.password) {
        res.json({success: false, msg: 'Please pass username and password.'});
    } else if(req.body.username == "" || req.body.password == "") {
        res.json({success: false, msg: 'Username or password cannot be blank.'});
    } else {
        var newUser = new User({
            username: req.body.username,
            password: req.body.password,
            firstname: (req.body.firstname ? req.body.firstname : ''),
            lastname: (req.body.lastname ? req.body.lastname : '')
        });
        // save the user
        newUser.save(function(err) {
            if (err) {
                return res.json({success: false, msg: 'Username already exists.'});
            }
            res.json({success: true, msg: 'User created successfully.'});
        });
    }
}

module.exports = router;