import React, { Component } from 'react'
import 'whatwg-fetch'
import Navbar from '../Navbar';
import { verifyAuthenticated, checkVar, getCurrentUser } from '../utils/utils'
import * as stateChanges from '../utils/stateChanges'
import { Link } from 'react-router-dom';

class ManageUser extends Component {
    constructor (props) {
        super(props);
        this.state = {
            'user': {
                _id: null,
                firstname: '',
                lastname: '',
                email: '',
                password: ''
            },
            'message': {
                'message': '',
                'className': 'alert-warning'
            }
        };

        this.getUser = this.getUser.bind(this)

    }

    componentDidMount() {
        //verifyAuthenticated(
        //    () => {
                let { match } = this.props
                if(match.hasOwnProperty("params") && match.params.hasOwnProperty("userId")) {
                    this.getUser(match.params.userId)
                }
        //    },
        //    () => this.props.history.push("/login")
        //)
    }

    onChange = (e) => {
        const { user } = this.state
        user[e.target.name] = e.target.value;
        this.setState({user: user});
    }

    onSubmit = (e) => {
        e.preventDefault()
        if(this.isEditing()) {
            this.updateUser()
        } else {
            this.createUser()
        }
    }

    getUser(userId) {
        let that = this;

        if(userId !== null) {
            fetch('/api/users/' + userId, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "Authorization": localStorage.getItem('jwtToken')
                }
            }).then(
                response => response.json()
            ).then(json => {
                if(json.hasOwnProperty("success") && json.success && json.hasOwnProperty("results")) {
                    that.setState(stateChanges.user(json.results))
                } else {
                    throw json
                }
            }).catch(error => {
                if(error) {
                    that.setState(stateChanges.message({
                        message: error.hasOwnProperty("msg") ? error.msg : 'Error',
                        className: 'alert-warning'
                    }));
                }
            })
        }
    }

    createUser() {
        let that = this;
        const {user } = this.state;

        return fetch('/api/users', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": localStorage.getItem('jwtToken')
            },
            "body": JSON.stringify(user)
        }).then(
            response => response.json()
        ).then(json => {
            if(json.hasOwnProperty("success") && json.success && json.hasOwnProperty("msg")) {
                this.props.history.push('/users')
            } else {
                throw json
            }
        }).catch(error => {
            if(error) {
                that.setState(stateChanges.message({
                    message: error.hasOwnProperty("msg") ? error.msg : 'Error',
                    className: 'alert-warning'
                }));
            }
        })
    }

    updateUser() {
        let that = this;
        let { user } = this.state

        if(user.hasOwnProperty("_id")) {
            fetch('/api/users/' + user._id, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "Authorization": localStorage.getItem('jwtToken')
                },
                body: JSON.stringify(user)
            }).then(
                response => response.json()
            ).then(json => {
                if(json.hasOwnProperty("success") && json.success && json.hasOwnProperty("msg")) {
                    this.props.history.push({
                        pathname: '/users',
                        state: { message: json.msg }
                    })
                } else {
                    throw json
                }
            }).catch(error => {
                if(error) {
                    that.setState(stateChanges.message({
                        message: error.hasOwnProperty("msg") ? error.msg : 'Error',
                        className: 'alert-warning'
                    }));
                }
            })
        }
    }

    isEditing() {
        let { match } = this.props
        return (match.hasOwnProperty("params") && match.params.hasOwnProperty("userId"))
    }

    render() {
        let { user, message } = this.state

        let currentUser = getCurrentUser()

        let passwordField = ''
        if(!this.isEditing()) {
            passwordField = (
                <React.Fragment>
                    <div className="form-group required">
                        <label className="control-label required" htmlFor="password">Password</label>
                        <input type="password" id="password" name="password" placeholder="Password..." maxLength="255" className="form-control" value={user.password} onChange={this.onChange} required />
                    </div>
                </React.Fragment>
            )
        }

        return (
            <div>
                <Navbar />
                <div className="container">
                    <div className="col-md-6 col-md-offset-3">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">
                                    Account Info{(this.isEditing() ? ': ' + user.firstname + " " + user.lastname : '')}
                                </h3>
                            </div>
                            <div className="panel-body">
                                {message.message !== '' &&
                                <div className={`alert ${message.className} alert-dismissible`} role="alert">
                                    { message.message }
                                </div>
                                }

                                <div className="col-xs-6 marginBottom10 noLRPadding">
                                    <span className="required-message">indicates required field</span>
                                </div>
                                <div className="col-xs-6 text-right marginBottom10 noLRPadding">
                                    <Link to="/users">Back to Users</Link>
                                </div>

                                <form onSubmit={this.onSubmit}>
                                    <div className="form-group required">
                                        <label className="control-label required" htmlFor="firstname">First Name</label>
                                        <input type="text" id="firstname" name="firstname" placeholder="First Name..." maxLength="255" className="form-control" value={user.firstname} onChange={this.onChange} required autoFocus={!this.isEditing()} />
                                    </div>
                                    <div className="form-group required">
                                        <label className="control-label required" htmlFor="lastname">Last Name</label>
                                        <input type="text" id="lastname" name="lastname" placeholder="Last Name..." maxLength="255" className="form-control" value={user.lastname} onChange={this.onChange} required />
                                    </div>
                                    <div className="form-group required">
                                        <label className="control-label required" htmlFor="username">Email Address</label>
                                        <input type="email" id="username" name="username" placeholder="Email Address..." maxLength="255" className="form-control" value={user.username} onChange={this.onChange} required readOnly={(checkVar(currentUser.username) !== null && currentUser.username == user.username)} />
                                    </div>
                                    {passwordField}
                                    <button className="btn btn-lg btn-primary btn-block" type="submit">{this.isEditing() ? "Update" : "Create"}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ManageUser;
