import React, { Component } from 'react';
import 'whatwg-fetch'
import './Login.css';
import AnonLayout from './AnonLayout'
import { Link } from 'react-router-dom';
import * as stateChanges from '../utils/stateChanges'

class Login extends Component {

    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            'message': {
                'message': '',
                'className': 'alert-warning'
            }
        };
    }

    componentDidMount() {
        localStorage.removeItem('jwtToken')
        localStorage.removeItem('selectedSchool')

        //Display message if it exists in the location state
        if(this.props.location.hasOwnProperty("state") && typeof this.props.location.state !== 'undefined' && this.props.location.state.hasOwnProperty("message"))
        {
            this.flashMessage(this.props.location.state.message, () => {
                //Remove the message from location state so it isn't displayed again
                this.props.history.replace({
                    pathname: this.props.location.pathname,
                    state: {}
                })
            })
        }
    }

    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();

        const { username, password } = this.state;
        let that = this;

        fetch('/api/auth/login', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            "body": JSON.stringify({
                username: username,
                password: password
            })
        }).then(
            response => response.json()
        ).then(result => {
            if(result.hasOwnProperty("success") && result.success && result.hasOwnProperty("token")) {
                localStorage.setItem('jwtToken', result.token);
                this.setState({ message: '' });
                this.props.history.push('/')
            } else {
                throw result
            }
        }).catch(error => {
            that.setState(stateChanges.message({
                message: error.hasOwnProperty("msg") ? error.msg : 'Error',
                className: 'alert-warning'
            }));
        })
    }

    flashMessage(message, timeoutCallback) {
        //Update state to the new message
        this.setState(stateChanges.message({
            message: message,
            className: 'alert-success'
        }), () => {
            //After state is updated, set a timeout for 2 seconds
            setTimeout(() => {
                timeoutCallback()
                //Remove the message from state so it isn't displayed
                this.setState(stateChanges.message({
                    message: ''
                }))
            }, 2000)
        });
    }

    render() {
        const { username, password, message } = this.state;

        return (
            <AnonLayout top={<h2 className="text-center">College Scorecard</h2>}>
                <h3 style={{fontWeight: 'normal'}}>Login</h3>
                {message.message !== '' &&
                <div className={`alert ${message.className} alert-dismissible`} role="alert">
                    { message.message }
                </div>
                }

                <div className="col-xs-12 loginFormMiddle">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label className="sr-only" htmlFor="username">Email Address</label>
                            <input type="email" className="form-control input-lg emailAddressTextbox" placeholder="Email Address..." id="username" name="username" value={username} onChange={this.onChange} tabIndex="1" required autoFocus />
                        </div>
                        <div className="form-group">
                            <label className="sr-only" htmlFor="password">Password</label>
                            <input type="password" id="password" className="form-control input-lg passwordTextbox" placeholder="Password..." name="password" value={password} onChange={this.onChange} tabIndex="2" required/>
                        </div>
                        <div className="form-group col-xs-4 noLRPadding">
                            <Link to="/register" className="link">Register</Link>
                        </div>
                        <div className="form-group col-xs-8 text-right noLRPadding">
                        </div>
                        <button type="submit" className="btn btn-block btn-lg btn-primary">Log In</button>
                    </form>
                </div>
            </AnonLayout>
        );
    }
}

export default Login;