import React, { PureComponent } from 'react';
import { checkVar } from '../utils/utils'
import { Link } from 'react-router-dom';

class UserTable extends PureComponent {
    render() {
        let { users, handleDelete, currentUser } = this.props

        checkVar(currentUser.username)

        let userRows = users.map(function(user){
            return (
                <tr key={user._id}>
                    <td>{user._id}</td>
                    <td>{(user.hasOwnProperty("username") ? user.username : '')}</td>
                    <td>{(user.hasOwnProperty("firstname") ? user.firstname : '')}</td>
                    <td>{(user.hasOwnProperty("lastname") ? user.lastname : '')}</td>
                    <td>
                        <div className="btn-group">
                            <Link className="btn btn-xs btn-default" to={`/users/update/${user._id}`}>Edit</Link>
                            {(checkVar(currentUser.username) !== null && currentUser.username !== user.username ? <button className="btn btn-xs btn-danger" onClick={() => handleDelete(user._id) }>Delete</button> : '')}
                        </div>
                    </td>
                </tr>
            )
        })

        return (
            <table className="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    {userRows}
                </tbody>
            </table>
        );
    }
}

export default UserTable;