import React, { Component } from 'react';
import 'whatwg-fetch'
import AnonLayout from './AnonLayout'
import { Link } from 'react-router-dom';
import * as stateChanges from '../utils/stateChanges'

class Register extends Component {

    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            firstname: '',
            lastname: '',
            'message': {
                'message': '',
                'className': 'alert-warning'
            }
        };
    }

    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();

        const { username, password, firstname, lastname } = this.state;
        let that = this;

        return fetch('/api/users', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            "body": JSON.stringify({
                username: username,
                password: password,
                firstname: firstname,
                lastname: lastname
            })
        }).then(
            response => response.json()
        ).then(json => {
            if(json.hasOwnProperty("success") && json.success && json.hasOwnProperty("msg")) {
                this.props.history.push({
                    pathname: '/login',
                    state: { message: json.msg }
                })
            } else {
                throw json
            }
        }).catch(error => {
            that.setState(stateChanges.message({
                message: error.hasOwnProperty("msg") ? error.msg : 'Error',
                className: 'alert-warning'
            }));
        })
    }

    render() {
        const { username, password, firstname, lastname, message } = this.state;
        return (
            <AnonLayout top={<h2 className="text-center">College Scorecard</h2>}>
                <div className="col-xs-6">
                    <h3 style={{fontWeight: 'normal'}}>Register</h3>
                </div>
                <div className="col-xs-6 text-right">
                    <Link to="/login" className="link">Back to Login</Link>
                </div>

                <div className="col-md-12">
                    {message.message !== '' &&
                    <div className={`alert ${message.className} alert-dismissible`} role="alert">
                        { message.message }
                    </div>
                    }
                </div>

                <div className="col-xs-12 loginFormMiddle">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label className="sr-only" htmlFor="firstname">First Name</label>
                            <input type="text" className="form-control input-lg" placeholder="First Name..." id="firstname" name="firstname" value={firstname} onChange={this.onChange} tabIndex="1" required autoFocus />
                        </div>
                        <div className="form-group">
                            <label className="sr-only" htmlFor="lastname">Last Name</label>
                            <input type="text" className="form-control input-lg" placeholder="Last Name..." id="lastname" name="lastname" value={lastname} onChange={this.onChange} tabIndex="1" required />
                        </div>
                        <div className="form-group">
                            <label className="sr-only" htmlFor="username">Email Address</label>
                            <input type="email" className="form-control input-lg emailAddressTextbox" placeholder="Email Address..." id="username" name="username" value={username} onChange={this.onChange} tabIndex="1" required />
                        </div>
                        <div className="form-group">
                            <label className="sr-only" htmlFor="password">Password</label>
                            <input type="password" id="password" className="form-control input-lg passwordTextbox" placeholder="Password..." name="password" value={password} onChange={this.onChange} tabIndex="2" required />
                        </div>
                        <div className="form-group col-xs-4 noLRPadding">

                        </div>
                        <div className="form-group col-xs-8 text-right noLRPadding">
                        </div>
                        <button type="submit" className="btn btn-block btn-lg btn-primary">Register</button>
                    </form>
                </div>
            </AnonLayout>
        );
    }
}

export default Register;