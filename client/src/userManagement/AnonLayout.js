import React, { Component } from 'react';
import 'whatwg-fetch'
import './Login.css';
import BodyColor from '../BodyColor'

class AnonLayout extends Component {

    render() {
        const { top, children } = this.props;

        return (
            <BodyColor isDark={true}>
                <div className="row">
                    <div className="col-md-6 col-md-offset-3 col-xs-10 col-xs-offset-1 form-box">
                        <div className="row loginFormTopSection">
                            <div className="col-sm-12 loginFormTopLeft">
                                {top}
                            </div>
                        </div>
                        <div className="row loginFormBottomSection">
                            {children}
                        </div>
                    </div>
                </div>
            </BodyColor>
        );
    }
}

export default AnonLayout;