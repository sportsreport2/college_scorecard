import React, { Component } from 'react'
import 'whatwg-fetch'
import Navbar from '../Navbar';
import { verifyAuthenticated, getCurrentUser } from '../utils/utils'
import { Link } from 'react-router-dom';
import * as stateChanges from '../utils/stateChanges'
import UserTable from './UserTable'

class Users extends Component {
    constructor (props) {
        super(props);
        this.state = {
            'users': [],
            'message': {
                'message': '',
                'className': 'alert-warning'
            },
            'allowUserCreation': false
        };

        this.getUsers = this.getUsers.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
    }

    componentDidMount() {
        let that = this
        //verifyAuthenticated(
        //    () => {
                //Display message if it exists in the location state
                if(that.props.location.hasOwnProperty("state") && typeof that.props.location.state !== 'undefined' && that.props.location.state.hasOwnProperty("message"))
                {
                    that.flashMessage(that.props.location.state.message, () => {
                        //Remove the message from location state so it isn't displayed again
                        that.props.history.replace({
                            pathname: that.props.location.pathname,
                            state: {}
                        })
                    })
                }

                //Pull the user list
                that.getUsers()
        //    },
        //    () => that.props.history.push("/login")
        //)
    }

    getUsers() {
        let that = this;
        fetch('/api/users', {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": localStorage.getItem('jwtToken')
            }
        }).then(
            response => response.json()
        ).then(json => {
            if(json.hasOwnProperty("success") && json.success && json.hasOwnProperty("results")) {
                that.setState(stateChanges.users(json.results))
            } else {
                throw json
            }
        }).catch(error => {
            if(error) {
                that.setState(stateChanges.message({
                    message: error.hasOwnProperty("msg") ? error.msg : 'Error',
                    className: 'alert-warning'
                }));
            }
        })
    }

    deleteUser(userId) {
        let that = this;
        fetch('/api/users/' + userId, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": localStorage.getItem('jwtToken')
            }
        }).then(
            response => response.json()
        ).then(json => {
            if(json.hasOwnProperty("success") && json.success && json.hasOwnProperty("msg")) {
                that.flashMessage(json.msg, () => {})
                that.getUsers()
            } else {
                throw json
            }
        }).catch(error => {
            if(error) {
                that.setState(stateChanges.message({
                    message: error.hasOwnProperty("msg") ? error.msg : 'Error',
                    className: 'alert-warning'
                }));
            }
        })
    }

    handleDelete(userId) {
        if(window.confirm('Are you sure you want to delete this user?')) {
            this.deleteUser(userId)
        }
    }

    flashMessage(message, timeoutCallback) {
        //Update state to the new message
        this.setState(stateChanges.message({
            message: message,
            className: 'alert-success'
        }), () => {
            //After state is updated, set a timeout for 2 seconds
            setTimeout(() => {
                timeoutCallback()
                //Remove the message from state so it isn't displayed
                this.setState(stateChanges.message({
                    message: ''
                }))
            }, 2000)
        });
    }

    render() {
        let { message, users, allowUserCreation } = this.state

        let addUserBtn = ''
        if(allowUserCreation) {
            addUserBtn = <div className="row">
                <div className="col-md-6">
                    <Link className="btn btn-sm btn-success" style={{'marginBottom': '10px'}} to="/users/create">Add New User</Link>
                </div>
            </div>
        }

        return (
            <div>
                <Navbar />
                <div className="container">
                    {message.message !== '' &&
                        <div className="row">
                            <div className="col-md-12 text-center">
                                <div className={`alert ${message.className} alert-dismissible`} role="alert">
                                    { message.message }
                                </div>
                            </div>
                        </div>
                    }
                    {addUserBtn}
                    <div className="row">
                        <div className="col-md-12">
                            <UserTable users={users} currentUser={getCurrentUser()} handleDelete={this.handleDelete} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Users;
