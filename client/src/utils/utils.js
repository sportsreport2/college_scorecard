export const verifyAuthenticated = (callback, errorCallback) => {
    fetch('/api/auth/verify', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": localStorage.getItem('jwtToken')
        }
    }).then(
        response => response.json()
    ).then(result => {
        if(result.hasOwnProperty("success") && result.success) {
            callback()
        } else {
            throw result
        }
    }).catch(error => {
        if(error) {
            errorCallback()
        }
    })
}

export const flashMessage = (setState, stateChanges, message, timeoutCallback) => {
    //Update state to the new message
    setState(stateChanges.message({
        message: message,
        className: 'alert-success'
    }), () => {
        //After state is updated, set a timeout for 2 seconds
        setTimeout(() => {
            timeoutCallback()
            //Remove the message from state so it isn't displayed
            setState(stateChanges.message({
                message: ''
            }))
        }, 2000)
    });
}

export const getLargestNumFromObjectKeys = object => {
    return Object.keys(object).reduce(function(result, key){
        return (!isNaN(key) && key > result ? key : result)
    })
}

export const checkVar = (item, defaultReturnValue) => {
    defaultReturnValue = defaultReturnValue || null
    try {
        return (item !== defaultReturnValue ? item : defaultReturnValue)
    } catch (e) {
        return defaultReturnValue
    }
}

export const getCurrentUser = () => {
    let jwtToken = localStorage.getItem("jwtToken")
    if(jwtToken !== null) {
        return parseJwt(jwtToken)
    }
    return {}
}

export const parseJwt = token => {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
};