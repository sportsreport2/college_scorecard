
export const autocompleteValue = (value) => (state) => ({
    autocompleteValue: value
});

export const selectedSchool = (selectedSchool) => (state) => ({
    selectedSchool: selectedSchool
});

export const schoolInfo = (schoolInfo) => (state) => ({
    schoolInfo: schoolInfo
});

export const schools = (schools) => (state) => ({
    schools: schools
});

export const users = (users) => (state) => ({
    users: users
});

export const user = (user) => (state) => ({
    user: user
});

export const userId = (userId) => (state) => ({
    userId: userId
});

export const message = (message) => (state) => ({
    message: message
});

export const errors = (errors) => (state) => ({
    errors: errors
});