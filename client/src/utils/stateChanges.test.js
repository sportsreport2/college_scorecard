import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils'; // ES6
import * as stateChanges from './stateChanges'

test('autocomplete value', () => {
    expect(stateChanges.autocompleteValue('Boston')({})).toEqual({'autocompleteValue':'Boston'});
});

test('selected school', () => {
    expect(stateChanges.selectedSchool('Boston College')({})).toEqual({'selectedSchool':'Boston College'});
});

test('school info', () => {
    expect(stateChanges.schoolInfo([{
        "1996": {
            "completion": {}
        }
    }])({})).toEqual({'schoolInfo':[{
        "1996": {
            "completion": {}
        }
    }]});
});

test('schools', () => {
    expect(stateChanges.schools([
        {
            'id': 1,
            'school.name': 'Boston College'
        },
        {
            'id': 2,
            'school.name': 'Boston University'
        }
    ])({})).toEqual({'schools':[
        {
            'id': 1,
            'school.name': 'Boston College'
        },
        {
            'id': 2,
            'school.name': 'Boston University'
        }
    ]});
});

test('errors', () => {
    expect(stateChanges.errors(['error1', 'error2'])({})).toEqual({errors: ['error1', 'error2']});
});