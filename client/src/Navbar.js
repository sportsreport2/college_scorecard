import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import NavLink from './NavLink'

class Navbar extends PureComponent {
    render() {
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarCollapse" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <Link className="navbar-brand" to="/">College Scorecard</Link>
                    </div>
                    <div className="collapse navbar-collapse" id="navbarCollapse">
                        <ul className="nav navbar-nav">
                            <NavLink pathName="/">Home</NavLink>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <NavLink pathName="/users">Users</NavLink>
                            <li>
                                <Link to="/login">Logout</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Navbar;