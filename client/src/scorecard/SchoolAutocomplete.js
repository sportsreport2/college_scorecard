import React, { Component } from 'react';
import Autocomplete from 'react-autocomplete'

class SchoolAutocomplete extends Component {
    render() {
        let {schools, autocompleteValue, handleSchoolChange, handleSchoolSelection} = this.props

        return (
            <div className="form-group">
                <label style={{'fontSize': '15px'}}>Type in a school name and select one to view the scorecard</label>
                <Autocomplete
                    wrapperStyle={{}}
                    menuStyle={{
                        borderRadius: '3px',
                        boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                        background: 'rgba(255, 255, 255, 0.9)',
                        padding: '2px 0',
                        fontSize: '100%',
                        position: 'fixed',
                        overflow: 'auto',
                        zIndex: 1000000,
                        maxHeight: '50%' // TODO: don't cheat, let it flow to the bottom
                    }}
                    inputProps={{
                        'className': 'form-control input-lg',
                        'autoFocus': true
                    }}
                    getItemValue={(item) => item['school.name']}
                    shouldItemRender={(item, value) => item['school.name'].toLowerCase().indexOf(value.toLowerCase()) > -1}
                    items={schools}
                    renderItem={(item, isHighlighted) =>
                    <div style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
                        {item['school.name']}
                    </div>
                }
                    value={autocompleteValue}
                    onChange={handleSchoolChange}
                    onSelect={handleSchoolSelection}
                />
            </div>
        );
    }
}

export default SchoolAutocomplete;
