import React, { Component } from 'react';
import AmCharts from "@amcharts/amcharts3-react";

class NetPriceByIncomeLevelGraph extends Component {

    render() {
        let {priceByIncomeLevel} = this.props

        let panelBody = "No data to populate the price by income level graph"
        if(priceByIncomeLevel !== null && Object.keys(priceByIncomeLevel).length > 0) {
            let graphData = Object.keys(priceByIncomeLevel).reduce((result, incomeLevel) => {
                if(priceByIncomeLevel[incomeLevel] !== null && priceByIncomeLevel[incomeLevel] > 0) {
                    result.push({
                        'incomeLevel': incomeLevel,
                        'price': priceByIncomeLevel[incomeLevel]
                    })
                }
                return result
            }, [])

            if (graphData.length > 0) {
                panelBody = (
                    <AmCharts.React
                        className="my-class"
                        style={{
                        width: "100%",
                        height: "500px"
                    }}
                        options={{
                        "type": "serial",
                        "theme": "light",
                        "valueAxes": [{
                            "axisAlpha": 0,
                            "position": "left",
                            "title": "Price"
                        }],
                        "startDuration": 1,
                        "graphs": [{
                            "balloonText": "<b>[[category]]: [[value]]</b>",
                            "fillColorsField": "color",
                            "fillAlphas": 0.9,
                            "lineAlpha": 0.2,
                            "type": "column",
                            "valueField": "price"
                        }],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "incomeLevel",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "labelRotation": 45,
                            "title": "Income Level"
                        },
                        "export": {
                            "enabled": true
                        },
                        "dataProvider": graphData
                    }}/>
                );
            }
        }

        return panelBody
    }
}

export default NetPriceByIncomeLevelGraph;
