import React, { Component } from 'react';
import AmCharts from "@amcharts/amcharts3-react";

class RaceEthnicityGraph extends Component {

    render() {
        let {raceEthnicity} = this.props

        let panelBody = "No data to populate the race / ethnicity graph"
        if(raceEthnicity !== null && Object.keys(raceEthnicity).length > 0) {
            let graphData = Object.keys(raceEthnicity).reduce((result, name) => {
                if(raceEthnicity[name] !== null && raceEthnicity[name] > 0) {
                    result.push({
                        'name': name,
                        'percentage': raceEthnicity[name]
                    })
                }
                return result
            }, [])

            if(graphData.length > 0) {
                panelBody = (
                    <AmCharts.React
                        className="my-class"
                        style={{
                        width: "100%",
                        height: "500px"
                    }}
                        options={{
                        "type": "pie",
                        "theme": "light",
                        "valueField": "percentage",
                        "titleField": "name",
                        "balloon":{
                            "fixedPosition":true
                        },
                        "export": {
                            "enabled": true
                        },
                        "dataProvider": graphData
                    }} />
                );
            }
        }

        return panelBody;
    }
}

export default RaceEthnicityGraph;
