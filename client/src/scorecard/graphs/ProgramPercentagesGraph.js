import React, { Component } from 'react';
import AmCharts from "@amcharts/amcharts3-react";

class ProgramPercentagesGraph extends Component {

    render() {
        let {programPercentages} = this.props

        let panelBody = "No data to populate the program percentages graph"
        if(programPercentages !== null && Object.keys(programPercentages).length > 0) {
            let graphData = Object.keys(programPercentages).reduce((result, program) => {
                if(programPercentages[program] !== null && programPercentages[program] > 0) {
                    result.push({
                        'program': program,
                        'percentage': programPercentages[program]
                    })
                }
                return result
            }, [])

            if(graphData.length > 0) {
                panelBody = (
                    <AmCharts.React
                        className="my-class"
                        style={{
                        width: "100%",
                        height: "500px"
                    }}
                        options={{
                        "type": "pie",
                        "theme": "light",
                        "valueField": "percentage",
                        "titleField": "program",
                        "balloon":{
                            "fixedPosition":true
                        },
                        "export": {
                            "enabled": true
                        },
                        "dataProvider": graphData
                    }} />
                );
            }
        }

        return panelBody;
    }
}

export default ProgramPercentagesGraph;
