import React, { Component } from 'react';
import AmCharts from "@amcharts/amcharts3-react";

class AttendanceByYearGraph extends Component {

    render() {
        let {schoolInfo} = this.props

        let panelBody = "No data to populate the attendance by year graph"
        let graphData = []
        if(schoolInfo.length > 0 && Object.keys(schoolInfo[0]).length > 0) {
            graphData = Object.keys(schoolInfo[0]).reduce((result, year) => {
                if(!isNaN(year)) {
                    let attendance = schoolInfo[0][year].cost.attendance.academic_year
                    if(attendance !== null && attendance > 0) {
                        result.push({
                            'year': year,
                            'attendance': attendance
                        })
                    }
                }
                return result;
            }, [])

            if (graphData.length > 0) {
                panelBody = (
                    <AmCharts.React
                        className="my-class"
                        style={{
                            width: "100%",
                            height: "500px"
                        }}
                        options={{
                            "type": "serial",
                            "theme": "light",
                            "marginRight": 80,
                            "autoMarginOffset": 20,
                            "marginTop": 7,
                            "dataProvider": graphData,
                            "valueAxes": [{
                                "axisAlpha": 0.2,
                                "dashLength": 1,
                                "position": "left"
                            }],
                            "graphs": [{
                                "id": "g1",
                                "balloonText": "[[value]]",
                                "bullet": "round",
                                "bulletBorderAlpha": 1,
                                "bulletColor": "#FFFFFF",
                                "hideBulletsCount": 50,
                                "title": "Attendance",
                                "valueField": "attendance",
                                "useLineColorForBulletBorder": true,
                                "balloon":{
                                    "drop":true
                                }
                            }],
                            "chartScrollbar": {
                                "autoGridCount": true,
                                "graph": "g1",
                                "scrollbarHeight": 40
                            },
                            "chartCursor": {
                                "limitToGraph":"g1"
                            },
                            "categoryField": "year",
                            "categoryAxis": {
                                "axisColor": "#DADADA",
                                "dashLength": 1,
                                "minorGridEnabled": true,
                                "title": "Year"
                            },
                            "export": {
                                "enabled": true
                            }
                        }}
                    />
                );
            }
        }

        return panelBody;
    }
}

export default AttendanceByYearGraph;
