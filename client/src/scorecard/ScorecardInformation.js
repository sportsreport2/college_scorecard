import React, { Component } from 'react';
import { getLargestNumFromObjectKeys, checkVar } from '../utils/utils'
import ExportToolbar from './ExportToolbar'
import ProgramPercentagesGraph from './graphs/ProgramPercentagesGraph'
import RaceEthnicityGraph from './graphs/RaceEthnicityGraph'
import NetPriceByIncomeLevelGraph from './graphs/NetPriceByIncomeLevelGraph'
import AttendanceByYearGraph from './graphs/AttendanceByYearGraph'

class ScorecardInformation extends Component {

    render() {
        let { schoolInfo, displayPDF, pdfExport, jsonExport } = this.props
        let latestYear = null;

        if(schoolInfo.length > 0) {
            latestYear = getLargestNumFromObjectKeys(schoolInfo[0])

            return (
                <div>
                    <ExportToolbar displayPDF={displayPDF} pdfExport={pdfExport} jsonExport={jsonExport} />

                    <div className="row" id="schoolInformation">
                        <div className="col-md-6">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h3 className="panel-title">School Information</h3>
                                </div>
                                <div className="panel-body">
                                    <table className="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>Name</td>
                                                <td>{checkVar(schoolInfo[0].school.name) + (checkVar(schoolInfo[0].school.alias) !== null ? " (" + schoolInfo[0].school.alias + ")" : "")}</td>
                                            </tr>
                                            <tr>
                                                <td>Website</td>
                                                <td>
                                                    {checkVar(schoolInfo[0].school.school_url) !== null ?
                                                        <a target="_blank" href={`http://${checkVar(schoolInfo[0].school.school_url)}`}>{checkVar(schoolInfo[0].school.school_url)}</a>
                                                        : ''
                                                    }
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>City, State Zip</td>
                                                <td>{checkVar(schoolInfo[0].school.city) + ", " + checkVar(schoolInfo[0].school.state) + " " + checkVar(schoolInfo[0].school.zip)}</td>
                                            </tr>
                                            <tr>
                                                <td>Total # of Students</td>
                                                <td>{checkVar(schoolInfo[0][latestYear].student.size)}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h3 className="panel-title">Program Percentages</h3>
                                </div>
                                <div className="panel-body">
                                    <ProgramPercentagesGraph programPercentages={checkVar(schoolInfo[0][latestYear].academics.program_percentage)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h3 className="panel-title">Race / Ethnicity</h3>
                                </div>
                                <div className="panel-body">
                                    <RaceEthnicityGraph raceEthnicity={checkVar(schoolInfo[0][latestYear].student.demographics.race_ethnicity)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h3 className="panel-title">Net Price by Income Level</h3>
                                </div>
                                <div className="panel-body">
                                    <NetPriceByIncomeLevelGraph priceByIncomeLevel={checkVar(schoolInfo[0][latestYear].cost.net_price.public.by_income_level)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h3 className="panel-title">Attendance by Year</h3>
                                </div>
                                <div className="panel-body">
                                    <AttendanceByYearGraph schoolInfo={schoolInfo} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return ('');
        }
    }
}

export default ScorecardInformation;
