import React, { Component } from 'react'
import 'whatwg-fetch'
import Navbar from '../Navbar';
import SchoolAutocomplete from './SchoolAutocomplete'
import ScorecardInformation from './ScorecardInformation'
import * as stateChanges from '../utils/stateChanges'
import html2canvas from 'html2canvas'
import { verifyAuthenticated, getLargestNumFromObjectKeys, checkVar } from '../utils/utils'
import './Scorecard.css';
import AmCharts from "@amcharts/amcharts3-react";

class Scorecard extends Component {
    constructor (props) {
        super(props);
        this.state = {
            'autocompleteValue': '',
            'schools': [],
            'selectedSchool': {}, //
            'schoolInfo': [],
            'message': {
                'message': '',
                'className': 'alert-warning'
            }
        };

        this.getSchools = this.getSchools.bind(this);
        this.getSchoolInfo = this.getSchoolInfo.bind(this);
        this.handleSchoolChange = this.handleSchoolChange.bind(this);
        this.handleSchoolSelection = this.handleSchoolSelection.bind(this);
        this.pdfExport = this.pdfExport.bind(this);
        this.jsonExport = this.jsonExport.bind(this);
        this.getDataUsed = this.getDataUsed.bind(this);
    }

    componentDidMount() {
        //verifyAuthenticated(
        //    () => {
                this.getSchools()
                //Set the selected school from localStorage. After state is updates, get the info about the selected school
                let selectedSchool = localStorage.getItem("selectedSchool")
                if(selectedSchool !== null) {
                    selectedSchool = JSON.parse(selectedSchool)
                    this.handleSchoolSelection(selectedSchool['school.name'], selectedSchool)
                }
        //    },
        //    () => this.props.history.push("/login")
        //)
    }

    //This function gets a list of schools filtered by name (autocomplete value)
    getSchools() {
        let that = this;
        fetch('https://api.data.gov/ed/collegescorecard/v1/schools?api_key=wn0Wcq4v6z8ebEsTjpa5buUDkqJQah4y3b7gYfUO&_fields=id,school.name&school.name=' + encodeURI(that.state.autocompleteValue), {
            method: "GET",
            "Content-Type": "application/json",
            "Accept": "application/json"
        }).then(
            response =>response.json()
        ).then(json => {
            if(json.hasOwnProperty("results")) {
                that.setState(stateChanges.schools(json.results))
            } else if (json.hasOwnProperty("error")) {
                throw json.error
            }
        }).catch(error => {
            that.setState(stateChanges.message({
                message: 'Error processing school request',
                className: 'alert-warning'
            }));
        })
    }

    //This function gets information about a selected school by ID
    getSchoolInfo() {
        if(this.state.selectedSchool.hasOwnProperty("id")) {
            let that = this;
            fetch('https://api.data.gov/ed/collegescorecard/v1/schools?api_key=wn0Wcq4v6z8ebEsTjpa5buUDkqJQah4y3b7gYfUO&id=' + encodeURI(that.state.selectedSchool.id), {
                method: "GET",
                "Content-Type": "application/json",
                "Accept": "application/json"
            }).then(
                response => response.json()
            ).then(json => {
                if(json.hasOwnProperty("results")) {
                    that.setState(stateChanges.schoolInfo(json.results))
                } else if (json.hasOwnProperty("error")) {
                    throw json.error
                }
            }).catch(error => {
                that.setState(stateChanges.message({
                    message: 'Error processing school info request',
                    className: 'alert-warning'
                }));
            })
        }
    }

    //This function is used when the school autocomplete text is changed
    handleSchoolChange = (e, val) => {
        //Set the value of the autocomplete
        this.setState(stateChanges.autocompleteValue(val))
        //If there is a school selected, remove it
        if(this.state.selectedSchool !== {}) {
            this.setState(stateChanges.selectedSchool({}))
        }
        //If there is info about the selected school, remove it
        if(this.state.schoolInfo.length > 0) {
            this.setState(stateChanges.schoolInfo([]))
        }
        //If there is a selectedSchool in local storage, remove it
        localStorage.removeItem('selectedSchool')

        //Get the list of schools
        this.getSchools()
    }

    //This function is used when a school is selected from the school autocomplete
    handleSchoolSelection = (val, item) => {
        //Set the value of the autocomplete
        this.setState(stateChanges.autocompleteValue(val))
        //Save selected school in localStorage
        localStorage.setItem('selectedSchool', JSON.stringify(item))
        //Set the selected school. After state is updates, get the info about the selected school
        this.setState(stateChanges.selectedSchool(item), () => this.getSchoolInfo())
    }

    pdfExport() {
        const input = document.getElementById('schoolInformation');
        html2canvas(input)
            .then((canvas) => {
                const img = canvas.toDataURL('image/jpeg');
                this.exportCharts([{
                    "image": img,
                    "fit": [ 523.28, 769.89 ]
                }])
            })
        ;
    }

    exportCharts(images) {
        // iterate through all of the charts and prepare their images for export
        var pending = AmCharts.charts.length;
        for ( var i = 0; i < AmCharts.charts.length; i++ ) {
            var chart = AmCharts.charts[ i ];
            chart.export.capture( {}, function() {
                this.toJPG( {
                    multiplier: 2
                }, function( data ) {
                    images.push( {
                        "image": data,
                        "fit": [ 523.28, 769.89 ]
                    } );
                    pending--;
                    if ( pending === 0 ) {
                        // all done - construct PDF
                        chart.export.toPDF( {
                            content: images
                        }, function( data ) {
                            this.download( data, "application/pdf", "amCharts.pdf" );
                        } );
                    }
                } );
            } );
        }
    }

    jsonExport() {
        if(this.state.selectedSchool.hasOwnProperty("id") && this.state.schoolInfo.length > 0) {
            let fileContents = this.getDataUsed()
            let schoolName = this.state.schoolInfo[0].school.name.toLowerCase().replace(/\s+/g, '_')
            let element = document.createElement("a");
            let file = new Blob([JSON.stringify(fileContents, null, 4)], {type: 'application/json'});
            element.href = URL.createObjectURL(file);
            element.download = schoolName + '.json';
            element.click();
        }
    }

    getDataUsed() {
        if(this.state.selectedSchool.hasOwnProperty("id") && this.state.schoolInfo.length > 0) {
            let schoolInfo = this.state.schoolInfo[0]
            let latestYear = getLargestNumFromObjectKeys(schoolInfo)
            let fileContents = {}

            if(schoolInfo.hasOwnProperty("school")) {
                fileContents.school = {}
                fileContents.school.name = checkVar(schoolInfo.school.name)
                if(schoolInfo.school.alias !== null) {
                    fileContents.school.alias = checkVar(schoolInfo.school.alias)
                }
                fileContents.school.school_url = checkVar(schoolInfo.school.school_url)
                fileContents.school.city = checkVar(schoolInfo.school.city)
                fileContents.school.state = checkVar(schoolInfo.school.state)
                fileContents.school.zip = checkVar(schoolInfo.school.zip)
                fileContents.school.number_of_students = checkVar(schoolInfo[latestYear].student.size)

            }

            let result;
            if(schoolInfo.hasOwnProperty(latestYear)) {
                let programPercentages = checkVar(schoolInfo[latestYear].academics.program_percentage, {})
                result = Object.keys(programPercentages).reduce((result, program) => {
                    if(programPercentages[program] !== null && programPercentages[program] > 0) {
                        result[program] = programPercentages[program]
                    }
                    return result
                }, {})

                if(Object.keys(result).length > 0) {
                    fileContents.program_percentage = result
                }

                let raceEthnicity = checkVar(schoolInfo[latestYear].student.demographics.race_ethnicity, {})
                result = Object.keys(raceEthnicity).reduce((result, name) => {
                    if(raceEthnicity[name] !== null && raceEthnicity[name] > 0) {
                        result[name] = raceEthnicity[name]
                    }
                    return result
                }, {})

                if(Object.keys(result).length > 0) {
                    fileContents.race_ethnicity = result
                }

                let priceByIncomeLevel = checkVar(schoolInfo[latestYear].cost.net_price.public.by_income_level, {})
                result = Object.keys(priceByIncomeLevel).reduce((result, incomeLevel) => {
                    if(priceByIncomeLevel[incomeLevel] !== null && priceByIncomeLevel[incomeLevel] > 0) {
                        result[incomeLevel] = priceByIncomeLevel[incomeLevel]
                    }
                    return result
                }, {})

                if(Object.keys(result).length > 0) {
                    fileContents.net_price_by_income_level = result
                }
            }

            result = Object.keys(schoolInfo).reduce((result, year) => {
                if(!isNaN(year)) {
                    let attendance = checkVar(schoolInfo[year].cost.attendance.academic_year)
                    if(attendance !== null && attendance > 0) {
                        result[year] = attendance
                    }
                }
                return result;
            }, {})

            if(Object.keys(result).length > 0) {
                fileContents.attendance_by_year = result
            }

            return fileContents
        }

        return {}
    }

    displayPDF() {
        let dataUsed = this.getDataUsed()
        //If data used has any of the 4 keys, it means there is data for at least one graph, so allow the PDF export
        return (dataUsed.hasOwnProperty("program_percentage")
            || dataUsed.hasOwnProperty("race_ethnicity")
            || dataUsed.hasOwnProperty("net_price_by_income_level")
            || dataUsed.hasOwnProperty("attendance_by_year")
        )
    }

    render() {
        let { message } = this.state

        return (
            <div>
                <Navbar />
                <div className="container">

                    <div className="row">
                        <div className="col-md-6 col-md-offset-3">
                            {message.message !== '' &&
                            <div class={`alert ${message.className} alert-dismissible`} role="alert">
                                { message.message }
                            </div>
                            }
                            <SchoolAutocomplete
                                schools={this.state.schools}
                                autocompleteValue={this.state.autocompleteValue}
                                handleSchoolChange={this.handleSchoolChange}
                                handleSchoolSelection={this.handleSchoolSelection}
                            />
                        </div>
                    </div>

                    <ScorecardInformation schoolInfo={this.state.schoolInfo} displayPDF={this.displayPDF()} pdfExport={this.pdfExport} jsonExport={this.jsonExport} />
                </div>
            </div>
        );
    }
}

export default Scorecard;
