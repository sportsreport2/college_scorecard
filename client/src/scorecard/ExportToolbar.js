import React, { PureComponent } from 'react';

class ExportToolbar extends PureComponent {
    render() {

        let { pdfExport, jsonExport, displayPDF } = this.props

        //Only allow PDF export if there is at least one graph
        //because PDF exporting is entirely handled by AmCharts

        return (
            <div className="row">
                <div className="col-md-12 text-right">
                    <div className="btn-group" role="group">
                        {(displayPDF ? <button type="button" className="btn btn-default" onClick={pdfExport}>PDF</button> : '')}
                        <button type="button" className="btn btn-default" onClick={jsonExport}>JSON</button>
                        <button type="button" className="btn btn-default" onClick={() => window.print()}>Print</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default ExportToolbar;