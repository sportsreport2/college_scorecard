import React, { PureComponent } from 'react';
import { matchPath } from 'react-router'
import { Link } from 'react-router-dom';

class NavLink extends PureComponent {
    render() {
        let { pathName, children } = this.props
        let active = matchPath(window.location.pathname, {
            path: pathName
        })

        return (
            <li className={(active !== null && active.hasOwnProperty("isExact") && active.isExact ? 'active' : '')}>
                <Link to={pathName}>{children}</Link>
            </li>
        );
    }
}

export default NavLink;