import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import PrivateRoute from './userManagement/PrivateRoute'
import Scorecard from './scorecard/Scorecard';
import Login from './userManagement/Login';
import Register from './userManagement/Register';
import Users from './userManagement/Users'
import ManageUser from './userManagement/ManageUser'
//import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Router>
        <div>
            <PrivateRoute exact path='/' component={Scorecard} />
            <Route path='/login' name="login" component={Login} />
            <Route path='/register' name="register" component={Register} />
            <Route
                path="/users"
                render={({ match: { url } }) => (
                  <div>
                    <PrivateRoute exact path={`${url}/`} component={Users} />
                    <PrivateRoute path={`${url}/create`} component={ManageUser} />
                    <PrivateRoute path={`${url}/update/:userId`} component={ManageUser} />
                  </div>
                )}
            />
        </div>
    </Router>,
    document.getElementById('root')
);
//registerServiceWorker();
