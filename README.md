

## Table of Contents

- [Requirements](#requirements)
- [Deviations from Requirements](#deviations-from-requirements)
- [Getting Started](#getting-started)
- [Possible Improvements](#possible-improvements)

## Requirements

**Goal**

The goal of this application was to build an app that users could log into, to view information about various universities in the United States, and manage users. The app allows a user to register and login. Then the user is able to type in the name of a school. A list of various schools displays based on the typed in characters. The user can choose a school from that list. Once a school is chosen, the user can view the following information about the school:

- A pie graph displaying the breakdown of various programs offered by the school
- A pie graph displaying the race/ethnicity breakdown of the school
- A bar chart displaying net price by income leven
- A line chart displaying attendance for all years that have data

A user can also view a list of registered users, edit users, and delete users. 

**Acceptance Criteria**

-   Must use one of the following languages: PHP, Node, React, Vue, Python
-   Use the api.data.gov API
-   Application must require authorization to view any information
- Display the following information about the selected school 
	-   Name and alias  
	-   Website
	-   City, State, Zip
	-   Total # of students
	-   A Donut chart of Program Percentages out of 100 (Only values of not null)
	-   A Donut chart of Race/Ethnicity (Only values of not null)
	-   A Graph of any type to display net price -> public->by income level
	-   Any Graph of your choosing for any other metrics in the API Call.
-   A button to save the page as a pdf
-   A button to download the data that populated the page
-   A button to print the page
-   An Admin portal for user management, should be able to update a user, add a user, and remove a user. 

## Deviations from Requirements

- A user can register for the application from the login page 
	- If an admin was adding users, there should be a way for the added user to create their password via a unique link emailed to them
	- The process of a user creating a password via a link in their email, was not implemented in this iteration of the application

## Getting Started 

#### **Quick Start**

- Navigate to https://college-scorecard-app.herokuapp.com/
- Register with your email address
- Log In

#### **Steps to run the application locally**
\*All commands below assume your current working directory is this project

Dependencies

- Node.js >= 6
- MongoDB

Installs dependencies for the client
```sh
cd client
npm install
```

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.
```sh
cd client
npm run build
```

Installs dependencies for the api
```sh
npm install
```

Starts the express server serving the client/build folder as well as API routes
```sh
NODE_ENV=production npm start
``` 

Navigate to localhost:5000

## Possible Improvements

- Link to public trello board to track progress https://trello.com/b/71dK604l/school-project

#### Front End Improvements
- Password
	- Add a confirm password text box to the register/create user form to make sure the user typed in the correct password
	- Set password requirements such as min length, at least one capital letter, at least one number, and at least one symbol
- Implement a state management tool such as flux or redux
	- The application is close to big enough to benefit from centralized state vs component specific state 
	- This would also allow for splitting up some components into even smaller components to increase maintainability 
- Improve protected routes by creating a parent component that handles authentication vs authenticating in each component
	- This would allow for implementing a redirect to the referer page after authentication
- Add sorting and pagination to the users table 
	- Return all results to the front end but only display 50 per page
	- Sorting to take place on the front end 
		- Each column could be sorted
	- Allow the user to decide the number of results per page
- Cache the search results in state so if a search is repeated, there is not an additional API call 
- Improve the autocomplete, possibly by switching packages, so the panel of names always appears underneath the textbox
- Improve PDF Export
	- Add a title
	- Include the bootstrap panels around the graphs to keep design consistent
	- Allow exporting of just the school information if there is no data to populate the graphs
- Use AmCharts License to remove branding
	- Modify labels on graph that come from the data.api.gov API to be more readable
- Add validation to all user management forms
	- Would be easy to leverage a react form builder that handles validation already
- Refactor code to be more scalable to allow for future improvements
- Leverage existing user management packages to allow for quicker development and scalability
- Leverage more ES6/ES7 improvements
- Move files in to a better directory structure to allow for scalability and readability

#### Back end Improvements
- Use an existing user management API that is proven to be highly secure, reliable, and configurable
	- This may mean not using node/express for the API 
- Enhance security to the API 
- Add a timeout 
- Move calls to the data.api.gov to the user defined API

#### General Improvements
- Improve unit tests
    - Test more functions of components from a react perspective
    - Add unit tests to the API
- Acceptance test on different browsers and screen sizes
	- Currently only tested on a 13'' screen in Chrome
- Add more comments
