module.exports = {
    'secret': process.env.SECRET,
    'mongodb_connection_string_prod': process.env.COLLEGE_SCORECARD_MONGO_CONNECTION_PROD,
    'mongodb_connection_string_dev': process.env.COLLEGE_SCORECARD_MONGO_CONNECTION_DEV
};