const funcs = {}

funcs.createUser = function(req, res, User, requireAuth) {
    requireAuth = requireAuth || false

    if(requireAuth) {
        var token = getToken(req.headers)
        if(!token) {
            return res.status(403).send({success: false, msg: 'Unauthorized.'});
        }
    }

    if (!req.body.username || !req.body.password) {
        res.json({success: false, msg: 'Please pass username and password.'});
    } else if(req.body.username == "" || req.body.password == "") {
        res.json({success: false, msg: 'Username or password cannot be blank.'});
    } else {
        var newUser = new User({
            username: req.body.username,
            password: req.body.password,
            firstname: (req.body.firstname ? req.body.firstname : ''),
            lastname: (req.body.lastname ? req.body.lastname : '')
        });
        // save the user
        newUser.save(function(err) {
            if (err) {
                return res.json({success: false, msg: 'Username already exists.'});
            }
            res.json({success: true, msg: 'User created successfully.'});
        });
    }
}

module.exports = funcs
